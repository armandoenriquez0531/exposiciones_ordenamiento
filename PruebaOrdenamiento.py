'''
Created on 22 nov. 2021

@author: arman
'''
import math

class QuickSort:
    def intercambia(self, A, x, y):
        tmp = A[x]
        A[x] = A[y]
        A[y] = tmp

    def Particionar(self, A, p, r):
        x = A[r]
        i = p - 1
        for j in range(p, r):
            if (A[j] <= x):
                i = i + 1
                self.intercambia (A, i, j)
        self.intercambia(A, i+1, r)
        return i + 1

    def QuickSort(self, A, p, r):
        if (p < r):
            q = self.Particionar(A, p, r)
            self.QuickSort(A, p, q-1)
            self.QuickSort(A, q+1, r)
        return A

    def ordenar(self, A):
        p = 0
        r = len(A) - 1
        q = int((p + r) / 2)
        return self.QuickSort(A, p, r)

class ShellSort:
    def shellSort(self, alist):
        sublistcount = len(alist) // 2
        while sublistcount > 0:
            for start_position in range(sublistcount):
                self.gap_InsertionSort(alist, start_position, sublistcount)
            sublistcount = sublistcount // 2
        return alist

    def gap_InsertionSort(self, nlist, start, gap):
        for i in range(start + gap, len(nlist), gap):
            current_value = nlist[i]
            position = i
            while position >= gap and nlist[position - gap] > current_value:
                nlist[position] = nlist[position - gap]
                position = position - gap
            nlist[position] = current_value

class Radix:
    def counting_sort(self, A, digit, radix):
        
        B = [0]* len(A)
        C = [0]* int(radix)
        
        for i in range(0, len(A)):
            digit_of_Ai = (A[i]/radix ** digit)% radix
            C[int(digit_of_Ai)] = C[int(digit_of_Ai)] +1 
            
        for j in range(1, radix):
            C[j]= C[j]+ C[j-1]
            
        for m in range(len(A)-1, -1, -1):
            digit_of_Ai = (A[m]/radix ** digit)% radix
            C[int(digit_of_Ai)]= C[int(digit_of_Ai)]-1
            B[C[int(digit_of_Ai)]] = A[m]
        return B 
    
    def radix_sort(self, A, radix):
        
        k = max(A)
        
        output = A 
        
        digits = int(math.floor(math.log(k, radix)+1))
        for i in range(digits):
            output= self.counting_sort(output, i, radix)
            
        return output


print("Algoritmo QuickSort")
a = [78, 67, 34, 35, 89, 56]
print("Sin ordenar")
print (a)
b = QuickSort()
print("Ordenado")
print (b.ordenar(a))
print()

print("Algoritmo ShellSort ")
nlist = [14,46,43,27,57,41,45,21,70]
print("Sin ordenar")
print(nlist)
s = ShellSort()
print("Ordenado")
print(s.shellSort(nlist))
print()

print("Algoritmo Radix")
a = Radix()
A =[9,5,78,52,12,1,23,67]
print("Original")
print(A)
print("Ordenado")
print(a.radix_sort(A,10))